from flask import Flask, request

app = Flask(__name__)
app.url_map.strict_slashes = False


@app.route('/hello', methods=['GET'])
def hello_world():
    return 'Hello World!'


@app.route('/hi', methods=['POST'])
def echo_name():
    json_body = request.json
    name = json_body['name']
    return f'Hi {name}'


@app.before_request
def before_request():
    if 'serverless.event' in request.environ:
        print(request.environ['serverless.event'])


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
